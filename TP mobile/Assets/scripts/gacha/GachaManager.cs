using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public class GachaManager : MonoBehaviour
{

    [SerializeField] private GachaRate[] gacha;
    [SerializeField] private Transform parent, pos;
    [SerializeField] private GameObject charactercardgo;

    GameObject charactercard;

    ImagenPre card;

    public void Gacha()
    {
        if (charactercard == null)
        {
            charactercard = Instantiate(charactercardgo, pos.position, Quaternion.identity) as GameObject;
            charactercard.transform.SetParent(parent);
            charactercard.transform.localScale = new Vector3(1, 1, 1);
            card = charactercard.GetComponent<ImagenPre>();
        }
        
        int rnd = UnityEngine.Random.Range(1, 101);
        Debug.Log(rnd);
        for(int i=0; i<gacha.Length;i++)
            {
                if(rnd <= gacha[i].rate)
                {
                    card.carta = Reward(gacha[i].rarity);
                    return;
                }
            }
        


    }

    public int Rates(string rarity)
    {
        GachaRate gr = Array.Find(gacha, rt => rt.rarity == rarity);
        if(gr != null)
        {
            return gr.rate;
        }
        else
        {
            return 0;
        }
    }

    PreInfo Reward(string rarity)
    {
        GachaRate gr = Array.Find(gacha, rt => rt.rarity == rarity);
        PreInfo[] reward = gr.reward;

        int rnd = UnityEngine.Random.Range(0, reward.Length);
        return reward[rnd];
    }

}
#if UNITY_EDITOR
[CustomEditor(typeof(GachaManager))]

public class GachaEditor : Editor
{

    public int Common, Uncommon, Rare, Epic, Legend;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();

        GachaManager gm = (GachaManager)target;

        Common = EditorGUILayout.IntField("Common", (gm.Rates("Common") - gm.Rates("Uncommon")));
        Uncommon = EditorGUILayout.IntField("Ucommon", (gm.Rates("Ucommon") - gm.Rates("Rare")));
        Rare = EditorGUILayout.IntField("Rare", (gm.Rates("Rare") - gm.Rates("Epic")));
        Epic = EditorGUILayout.IntField("Epic", (gm.Rates("Epic") - gm.Rates("Lagendary")));
        Legend = EditorGUILayout.IntField("Legend", gm.Rates("Legendary"));

    }

}
#endif
