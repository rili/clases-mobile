using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ImagenPre : MonoBehaviour
{
    public PreInfo carta;

    [SerializeField] private Image img;
    [SerializeField] private TextMeshProUGUI nombre;
    [SerializeField] private TextMeshProUGUI estrellas;
                              

    // Start is called before the first frame update
    void Start()
    {
        if (carta != null)
        {
            img.sprite = carta.image;
            nombre.text = carta.name;
            estrellas.text = carta.stars;
        }
    }

    // Update is called once per frame
     void Update()
    {
        Start();
    }
}
