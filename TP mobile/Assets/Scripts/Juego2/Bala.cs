using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public static  bool gameover;
    public float Velocidad;
    // Start is called before the first frame update
    void Start()
    {
        gameover = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Mundo"))
        {
            transform.SetParent(collision.transform);
        }

        if (collision.gameObject.CompareTag("Bala"))
        {
            gameover = true;
        }
    }

}
