using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoMuerte : MonoBehaviour
{
   public GameOver3 gm;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            gm.Setup();
        }
    }
}
