using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public float Velocidad = 10f;
    public float Rebote = 0.5f; // The amount of damping to apply

    private float CurrentVelocidad = 0f;

    private void Update()
    {
        float rotationInput = Input.GetAxis("Horizontal");
        CurrentVelocidad = Mathf.Lerp(CurrentVelocidad, -rotationInput * Velocidad, Rebote);
        transform.Rotate(Vector3.forward, CurrentVelocidad * Time.deltaTime);
    }
}