using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Jugador : MonoBehaviour
{
    public float Fuerza = 10f;
    public float Gravedad = 1f;
    private Rigidbody rb;
    public static bool gameover = false;
    public GameOver1 gm;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.freezeRotation = true;
        gameover = false;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            rb.useGravity = true;
            rb.velocity = Vector3.up * Fuerza;
        }

    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
        gameover = true;
        gm.Setup();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstaculo"))
        {
            Destroy(gameObject);
            gameover = true;
            gm.Setup();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstaculo"))
        {
            Destroy(gameObject);
            gameover = true;
            gm.Setup();
        }
    }
    
}
