using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuboSpawner : MonoBehaviour
{
    [SerializeField] private float Tiempospawn = 2f;
    [SerializeField] private GameObject prefabTubo;
    [SerializeField] private int Cantidad = 5;
    [SerializeField] private float PosXSpawn = 12f;
    [SerializeField] private float MinYpos = -2f;
    [SerializeField] private float MaxYpos = 3f;
    private GameObject[] Tubos;
    private int ContadorTubo;
    private float TiempoPasado;

    private void Start()
    {
        Tubos = new GameObject[Cantidad];
        for (int i =0; i <Cantidad;i++)
        {
            Tubos[i] = Instantiate(prefabTubo);
            Tubos[i].SetActive(false);
        }
    }
    void Update()
    {
        TiempoPasado += Time.deltaTime;

        if (TiempoPasado > Tiempospawn)
        {
            SpawnObstacle();
        }
       
    }

    private void SpawnObstacle()
    {
        TiempoPasado = 0;

        float PosYSpawn = Random.Range(MinYpos, MaxYpos);
        Vector3 Spawnpos = new Vector3(PosXSpawn, PosYSpawn, 3.39f);
        Tubos[ContadorTubo].transform.position = Spawnpos;

        if (!Tubos[ContadorTubo].activeSelf)
        {
            Tubos[ContadorTubo].SetActive(true);
        }
        ContadorTubo++;

        if (ContadorTubo == Cantidad)
        {
            ContadorTubo = 0;
        }
    }

   

    //private void GenerarTubo()
    //{
        
    //    Vector3 posicionSpawn = transform.position + new Vector3(0, Random.Range(-RangoAltura, RangoAltura));
    //    GameObject nuevoTubo = Instantiate(prefabTubo, posicionSpawn, Quaternion.identity);

    //    Destroy(nuevoTubo, 10f);
    //}
}
