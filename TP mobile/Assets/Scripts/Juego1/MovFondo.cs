using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovFondo : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public bool iniciar = false;


    void Start()
    {
        iniciar = false;
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        //parar = Controller_Player.isColliding;

        if (Input.GetButtonDown("Jump"))
        {
            iniciar = true;
        }


        if (iniciar)
        {
            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
            }
        }

    }
}
